class Shop < ActiveRecord::Base
  validates_presence_of :name
  validates_presence_of :distance_weight, unless: :distance_actual
  validates_presence_of :distance_actual, unless: :distance_weight
  validates_presence_of :price_average, if: 'price_floor.nil? && price_ceiling.nil?'
  validates_presence_of :price_floor, :price_ceiling, unless: :price_average
end
