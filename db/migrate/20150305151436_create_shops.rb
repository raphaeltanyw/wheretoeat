class CreateShops < ActiveRecord::Migration
  def change
    create_table :shops do |t|
      t.string :name
      t.decimal :distance_actual
      t.string :distance_weight
      t.decimal :price_floor
      t.decimal :price_ceiling
      t.decimal :price_average

      t.timestamps null: false
    end
  end
end
