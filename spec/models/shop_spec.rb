require 'rails_helper'

RSpec.describe Shop, type: :model do
  context 'basic model checks' do
    it 'has a valid factory' do
      shop = create(:shop)
      expect(shop).to be_valid
    end
  end

  context 'validation checks' do
    it 'is invalid without a name' do
      shop = build(:shop, name: nil)
      shop.valid?
      expect(shop.errors[:name]).to include("can't be blank")
    end

    it 'is invalid without an actual distance and distance weight' do
      shop = build(:shop, distance_actual: nil, distance_weight: nil)
      shop.valid?
      expect(shop.errors[:distance_actual]).to include("can't be blank")
      expect(shop.errors[:distance_weight]).to include("can't be blank")
    end

    it 'is valid if there is an actual distance without a distance weight' do
      shop = build(:shop, distance_actual: 100, distance_weight: nil)
      shop.valid?
      expect(shop).to be_valid
    end

    it 'is valid if there is a distance weight without an actual distance' do
      shop = build(:shop, distance_weight: 100, distance_actual: nil)
      shop.valid?
      expect(shop).to be_valid
    end

    it 'is invalid without a price range floor, price range ceiling and average price' do
      shop = build(:shop, price_floor: nil, price_ceiling: nil, price_average: nil)
      shop.valid?
      expect(shop.errors[:price_floor]).to include("can't be blank")
      expect(shop.errors[:price_ceiling]).to include("can't be blank")
      expect(shop.errors[:price_average]).to include("can't be blank")
    end

    it 'is valid with an average price, but without price range floor and price range ceiling' do
      shop = build(:shop, price_floor: nil, price_ceiling: nil, price_average: 10)
      shop.valid?
      expect(shop).to be_valid
    end

    it 'is valid with a price range floor and price range ceiling but without an average price' do
      shop = build(:shop, price_floor: 3, price_ceiling: 30, price_average: nil)
      shop.valid?
      expect(shop).to be_valid
    end
  end
end
