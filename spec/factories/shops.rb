FactoryGirl.define do
  factory :shop do
    name "MyString"
    distance_actual "9.99"
    distance_weight "MyString"
    price_floor "9.99"
    price_ceiling "9.99"
    price_average "9.99"
  end

end
